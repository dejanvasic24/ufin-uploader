const fs = require("fs");
const api = require("./api");

// Please update these values as you need:
const taxGroupId = "9530783f-2769-4e47-9ec1-741799c3bbd3";
const taxReturnId = "a29a5ca8-9488-4cc5-94e2-ee3b28cbd859";
const token =
  "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImNOaU5DamJiNURPczBLMk5jWXR5TSJ9.eyJpc3MiOiJodHRwczovL3VmaW4tc3RhZ2luZy5hdS5hdXRoMC5jb20vIiwic3ViIjoiYXV0aDB8NWVhY2Y1ODVkM2M1NGUwYmVjOGVjZjA0IiwiYXVkIjoiaHR0cHM6Ly91ZmluLXN0YWdpbmcuaGVyb2t1YXBwLmNvbS9hcGkiLCJpYXQiOjE2MDY2NDQzNjYsImV4cCI6MTYwNjczMDc2NiwiYXpwIjoick1sY2VUdWN4bUQzaG8wVkRxWUdIOFl5NklSODZYNkgiLCJndHkiOiJwYXNzd29yZCJ9.fdG6R78hcJNdVSKs0ahOI8sJAYnEMHWcnOLDCT37wfWy6I2vVcOPSnH-OF7Y7L7VZF5h9Ot1THxLZY4-04x63dvu-70ghYXBmJyrmFzF8KVmCa7c0N2Mjsqq9lvK7xYwmzG2pPnnCKfOYspCDL4G3t6BB_fRXO0T2ERTsS4IfnH0C5ycZxzjAWKAdhmKE5MgR_mvrJ9zOj40WDIIswyy2TDYQ9lxVWpPhk9Ey-MFVObpLTaTit-ZrtJBZePp7ZduBkCxFEwGLKKIz3bTSagPmWXidgA7W2dE_69ckWVCSfEzfbgdr4Q9DVC-e2E8nOqx4WJFwsPNCfMXnkxit4aQcw";

const clientToken =
  "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlJUZzVSa014TVVVeU1UTkJNakJEUVVJNU5rWkVRVFV5TnpVMFJEbEVNVE5DT1RWRlFrTkROQSJ9.eyJpc3MiOiJodHRwczovL3VmaW4uYXUuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVmY2FjZTZjMzVkNzllMDA3NmEyZGI2YSIsImF1ZCI6Imh0dHBzOi8vdWZpbi5jb20uYXUvYXBpIiwiaWF0IjoxNjA3MTU2MTg2LCJleHAiOjE2MDcyNDI1ODYsImF6cCI6IjJBazM2N29aVDRGZnFTOHExMnNXMUk0TmRMZU01eEtUIiwic2NvcGUiOiJvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.RYYRS2X4wk5hZ3Sku0WbwdIPXPbnhrdqB3NnyGiSHkRMlSweFQljz9vCz4_eS8zWaG8nAENkokKIFjAhHhEOrCPPnrnAg9JcAXjwHA6leL9fNIaSVKQSqzywpW11vnUnbkqVgrVcXJoIeNXa5BGAMVEMid8SQeQXHPngl3mjVHw5UzmC3KWb_rBZ_zveQmskht2_7YgLyM76OsxsEZuUlDUJ2LC0q0C0t3uoCdgumUek3zlHPf2JfTw-gsYegnzPoMac4wr9H0QrHQkoHhTEqbd5NnepnZEwsoMZtc-OVWn1oEnDWLgX-xmKBe_OjUMZZ9iHVYEemH2ygGAIs0AyZw";

const createTaxGroupDocument = async (fileName) => {
  const filePath = `./files/${fileName}`;
  const fileData = fs.readFileSync(filePath);

  const { createFileUploadUrl } = await api.createFileUploadUrl({
    fileName,
    fileType: "TAX_GROUP_DOCUMENT",
    token: clientToken,
  });

  const { contentType } = await api.upload({
    signedUrl: createFileUploadUrl.signedRequest,
    fileData,
    fileName,
  });

  await api.createTaxGroupDocument({
    taxGroupId,
    contentType,
    fileName,
    token: clientToken,
    documentKey: createFileUploadUrl.documentKey,
  });

  console.log("Tax Document upload Completed successfully");
};

const createTaxReturnDocument = async (fileName) => {
  const filePath = `./files/${fileName}`;
  const fileData = fs.readFileSync(filePath);

  const { createFileUploadUrl } = await api.createFileUploadUrl({
    fileName,
    fileType: "TAX_RETURN_DOCUMENT",
    token,
  });

  const { contentType } = await api.upload({
    signedUrl: createFileUploadUrl.signedRequest,
    fileData,
    fileName,
  });

  await api.sendTaxDocument({
    token,
    taxReturnId,
    contentType,
    documentKey: createFileUploadUrl.documentKey,
    name: fileName,
  });

  console.log("Tax Return document upload completed successfully");
};

(async function () {
  await createTaxGroupDocument("TaxEstimate.pdf");
})();
