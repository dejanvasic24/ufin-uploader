const fs = require("fs");
const path = require("path");

const api = require("../api");

const taxGroupId = "f55570ab-82b1-4f0e-8e7a-063ffc4ffa1d";
const taxReturnId = "9f65bea2-cbdb-434e-9b53-c20a010852b4";
const clientToken =
  "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImNOaU5DamJiNURPczBLMk5jWXR5TSJ9.eyJpc3MiOiJodHRwczovL3VmaW4tc3RhZ2luZy5hdS5hdXRoMC5jb20vIiwic3ViIjoiYXV0aDB8NWY2OWQ2NzA0YjIzNTUwMDZjYzM0ZTIwIiwiYXVkIjoiaHR0cHM6Ly91ZmluLXN0YWdpbmcuaGVyb2t1YXBwLmNvbS9hcGkiLCJpYXQiOjE2MDczMjczNTAsImV4cCI6MTYwNzQxMzc1MCwiYXpwIjoick1sY2VUdWN4bUQzaG8wVkRxWUdIOFl5NklSODZYNkgiLCJndHkiOiJwYXNzd29yZCJ9.aTnUtIYMvSRTtjqxiSt8ukdJ_AsS_Fo54a_tPaxQAVOBZS1Qyi1FVMVdOtV_AwJrm83XyB-ST13W-c1XSPAeUkUemK_bWH4G8qpIcIzQmA8LjUTNTtrlf9p-xLfyCyeQ_q7Sn1FVMbiGln9k15niA0zBM6u1T5DkOSYBWJyriKc0-dOaUEQ5o2KvnWhCCrEoWPlwVqwLh4VfpGzFQbDLUODWfRaxkZJilVCHtCyfOBINzH4aSyg0NHLWcnfblQH45Gc5D0Mp5-m7uLJcf-5pGUuVlHRcQGk1u1qbQVsClMtrx3kUY_tOP1ioUG2wAbFLr18TvrZdTuhnEKZQf2kw-A";

const createTransaction = async (fileName) => {
  const filePath = path.resolve(__dirname, `../files/${fileName}`);
  const fileData = fs.readFileSync(filePath);

  const { createFileUploadUrl } = await api.createFileUploadUrl({
    fileName,
    fileType: "TAX_RECEIPT",
    token: clientToken,
  });

  const { contentType } = await api.upload({
    signedUrl: createFileUploadUrl.signedRequest,
    fileData,
    fileName,
  });

  const resp = await api.createTransacton({
    taxReturnId,
    token: clientToken,
    documentKey: createFileUploadUrl.documentKey,
    amount: -2000,
    description: "Optus Subscription",
    documentContentType: contentType,
    documentName: fileName,
    taxGroupId,
    transactionDate: new Date().toISOString(),
  });

  console.log("Tax Receipt Upload Completed successfully", resp.data);
};

(async function () {
  await createTransaction("Document.xlsx");
})();
