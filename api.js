const axios = require("axios");
const mime = require("mime-types");
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

const config = require("./config");
const queries = require("./queries");

const postGraphQL = async ({ token, data }) => {
  try {
    const resp = await axios.post(config.endpoint, data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    if (resp.data.errors) {
      console.log(resp.data.errors);
      throw new Error("API responded with error", resp.data.errors);
    }

    return resp.data.data;
  } catch (err) {
    throw err;
  }
};

module.exports.myTax = async ({ token }) => {
  console.log("Getting my tax");

  return await postGraphQL({
    token,
    data: {
      query: queries.myTax,
    },
  });
};

module.exports.createFileUploadUrl = async ({ fileName, fileType, token }) => {
  console.log("Getting the signed request for uploading", token);
  return await postGraphQL({
    token,
    data: {
      query: queries.createFileUploadUrl,
      variables: {
        input: {
          fileName,
          contentType: mime.lookup(`./${fileName}`),
          fileType,
        },
      },
    },
  });
};

module.exports.createTaxGroupDocument = async ({
  taxGroupId,
  documentKey,
  fileName,
  contentType,
  token,
}) => {
  console.log(
    `Assigning tax group document TaxGroupID: ${taxGroupId}, FileName: ${fileName}, DocumentKey: ${documentKey}`
  );
  const data = {
    query: queries.createTaxGroupDocument,
    variables: {
      input: {
        taxGroupId,
        documentKey,
        name: fileName,
        contentType,
      },
    },
  };

  return await postGraphQL({ token, data });
};

module.exports.createTransacton = async ({
  taxGroupId,
  taxReturnId,
  amount,
  description,
  transactionDate,
  documentKey,
  documentName,
  documentContentType,
  token,
}) => {
  const data = {
    query: queries.createTransacton,
    variables: {
      input: {
        taxGroupId,
        taxReturnId,
        amount,
        description,
        transactionDate,
        documentKey,
        documentName,
        documentContentType,
      },
    },
  };

  return await postGraphQL({ token, data });
};

module.exports.sendTaxDocument = async ({
  token,
  taxReturnId,
  documentKey,
  name,
  contentType,
}) => {
  console.log("Accountant is sending the tax document");
  const data = {
    query: queries.sendTaxDocument,
    variables: {
      input: {
        taxReturnId,
        documentKey,
        name,
        contentType,
        signatureRequired: false,
      },
    },
  };

  return await postGraphQL({
    token,
    data,
  });
};

module.exports.upload = async ({ signedUrl, fileData, fileName }) => {
  const contentType = mime.lookup(`./${fileName}`);

  const resp = await axios({
    url: signedUrl,
    method: "PUT",
    data: fileData,
    headers: {
      "Content-Type": contentType,
    },
  });

  if (resp.status !== 200) {
    throw new Error("Failed to upload the file");
  }

  return { contentType, fileName, fileData, signedUrl };
};
