module.exports.createTaxGroupDocument = `
  mutation CreateTaxGroupDocument($input: TaxGroupDocumentInput!) {
    createTaxGroupDocument(input: $input) {
        id
        taxGroupId
        documentKey
        name
        createdAt
        updatedAt
    } 
  }
`;

module.exports.createTransacton = `
  mutation CreateTransaction($input: TaxTransactionInput!) {
    createTransaction(input: $input) {
        id
        taxReturnId
        taxGroupId
        amount
        description
        transactionDate
        documentKey
        documentName
        documentContentType
        createdAt
        updatedAt
    } 
  }
`;

module.exports.createFileUploadUrl = `
  mutation CreateFileUploadUrl($input: FileUrlInput!) {
    createFileUploadUrl(input: $input) {
      signedRequest
      documentKey
      url
    }
  }
`;

module.exports.sendTaxDocument = `
  mutation SendTaxDocument($input: TaxReturnDocumentInput!) {
    sendTaxDocument(input: $input) {
        id
        taxReturnId
        name
        contentType
        documentKey
        signatureRequired
        signed
        createdAt
        updatedAt
    } 
  } 
`;

module.exports.myTax = `
  query {
    myTax(year: 2021) {
        id
        clientId
        accountantId
        year
        financialYear
        status
        submittedAt
        totalExpenses
        totalIncome
        numberOfExpenseItems
        groups {
            totalExpenses
            totalIncome
            totalReceipts
            taxGroup {
                id
                groupName
                groupType
                createdAt
            }
            taxReturn {
                id
                year 
                financialYear
            }
        }
    }
  }
`;
